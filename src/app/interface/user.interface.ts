export interface userTypeOfClass {
    id?: number,
    email?: string,
    roles?:[],
    firstName?: string,
    lastName?: string,
    numberAddress?: string,
    nameAddress?: string,
    complementAddress?: string,
    city?: string,


}


// {

//     "id": 1,
//     "email": "reichel.zetta@hotmail.com",
//     "roles": [
//           "ROLE_USER"
//     ],
//     "firstName": "Zora",
//     "lastName": "Steuber"

// }