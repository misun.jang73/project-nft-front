export interface nftTypeOfClass {
    id?: number,
    nameNft?: string,
    description?: string,
    pathURL?: string,
    priceEth?: number,
    user:[],
    subCategories:[],
}



/*
{

        "id": 202,
        "nameNFT": "koko",
        "description": "zklfhi zuify erpgpie ryg^ergy epirgu ergp",
        "pathURL": "/public/upload/seukeulinsyas-2023-07-20-151248-6502e228dfc23.png",
        "priceEth": 5

},
*/

